# WP Development POC
This repo is a POC for doing quick localdev environments managed with composer and docker.
By default it will create images to install wordpress/mysql and run gulp (or webpack).

## Usage
### Getting started
```bash
$ composer install # first time only
$ docker-compose up
```
### Accessing containers
To access a particular docker container, find the container name and then enter an interactive terminal.
```bash
$ docker ps # to get the container name
$ docker exec -it container_name /bin/bash
```
### Debugging Wordpress
Wordpress debug logs can be found inside the web container at `/etc/httpd/logs/error_log`

## Bitbucket

### Pipelines
This repo has a bitbucket pipelines (written by Adam Berkowitz) attached in case you wish to create zip file downloads for tags/branches. To enable pipelines on bitbucket, simply visit the project repo and enable pipelines at `repo -> settings -> Pipelines/settings`.

## Using wp-project-boilerplate for a theme/plugin

It would be a good idea to keep UComm/WordPress dependencies in require-dev, and only keep functional dependencies in "require".  That way, your package will export with only the required files for your plugin/theme to function, and not include a full WP install.

## Using wp-project-boilerplate for a site

It would be a good idea to move the components in composer.json "require-dev" to "require", and only what you need.  That way they will be packaged with the site.  You should develop themes and plugins separately from this site repository unless they aren't in composer.

## Known Issues
- The Docker images don't currently work under Linux. This is an issue related to ip tables.
- ~~Docker does not work. Need a working image for WordPress.~~
- ~~`www/wp-config.php` needs to be edited accordingly.~~
- After a release/version bump re-run the [Composer Repository Pipelines build](https://bitbucket.org/ucomm/composer-repository).
